import { connect } from 'react-redux'
import Home from '../components/Home'
import { AUTH_LOGOUT_REQUEST } from '../actions/types'

const mapStateToProps = state => ({
    loggedin: state.authentication.loggedin
})

const mapDispatchToProps = dispatch => ({
    onLogout: () => dispatch({ type: AUTH_LOGOUT_REQUEST })
})

const App = connect(mapStateToProps, mapDispatchToProps)(Home)

export default App
