import { combineReducers } from 'redux'
import { reducer as form } from 'redux-form'
import { reducers as authReducers } from '../auth'

const reducer = combineReducers({
    form,
    authentication: authReducers.authentication,
    loginForm: authReducers.loginForm
})

export default reducer
