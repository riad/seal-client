import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import createSagaMiddleware from 'redux-saga'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { Router, Route, IndexRoute, hashHistory, Redirect } from 'react-router'
import reducer from './reducers'
import rootSaga from './sagas'
import App from './containers/App'
import EmptyContainer from './components/EmptyContainer'
import ListInvoices from './components/Seller/ListInvoices'
import NewInvoice from './components/Seller/NewInvoice'
import Buyer from './components/Buyer'

let store = createStore(reducer, applyMiddleware(createSagaMiddleware(rootSaga)))

render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={App}>
        <Route path="/seller" component={EmptyContainer}>
          <IndexRoute component={ListInvoices} />
          <Route path="new-invoice" component={NewInvoice}/>
        </Route>
        <Route path="/buyer" component={Buyer}/>
        <Redirect from="/" to="/seller" />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)
