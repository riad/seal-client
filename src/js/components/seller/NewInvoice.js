import React from 'react'
import { IndexLink } from 'react-router'

const NewInvoice = () => (
  <section>
    <ol className="breadcrumb">
      <li>
        <IndexLink to="/seller">Espace vendeur</IndexLink>
      </li>
      <li className="active">
        <span>Nouvelle facture</span>
      </li>
    </ol>
  </section>
)

export default NewInvoice
