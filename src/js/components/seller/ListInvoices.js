import React from 'react'
import { Link } from 'react-router'

const ListInvoices = () => (
  <section>
    <div className="pull-right">
      <Link className="btn btn-primary-full" activeClassName="active" to="/seller/new-invoice">
        Créer une nouvelle facture
      </Link>
    </div>

    <ol className="breadcrumb">
      <li className="active">
        <span>Espace vendeur</span>
      </li>
    </ol>
  </section>
)

export default ListInvoices
