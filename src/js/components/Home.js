import React from 'react'
import { Router, Route, hashHistory, Link } from 'react-router'
import { createLoginForm, LogoutLink } from '../auth'
const LoginForm = createLoginForm('loginForm')

const Home = ({ loggedin, children }) => (
  <section>
    <header className="application-header">
      <a id="main-logo" className="logo">
        SEAL
      </a>
      <nav id="main-menu">
        <ul role="nav">
          <li><Link activeClassName="active" to="/buyer">Espace acheteur</Link></li>
          <li><Link activeClassName="active" to="/seller">Espace vendeur</Link></li>
        </ul>
      </nav>

      <div className="content pull-right">
        { loggedin ? <LogoutLink className="btn btn-primary"><i className="fa fa-lock" /></LogoutLink> : null }
      </div>
    </header>

    <div className="pad">
      { !loggedin ? <LoginForm /> : children }
    </div>
  </section>
)

export default Home
