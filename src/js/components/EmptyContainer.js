import React from 'react'
import { Link } from 'react-router'

const EmptyContainer = ({ children }) => (
  <section>
    {children}
  </section>
)

export default EmptyContainer
