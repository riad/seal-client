import { fork } from 'redux-saga/effects'
import { createAuthenticationSaga } from '../auth'
import config from '../config'

function* rootSaga() {
  yield fork(createAuthenticationSaga(config.server))
}

export default rootSaga
